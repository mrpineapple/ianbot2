import botlib,sys,scrapemark,linecache,random
import os
import unicodedata
             
##
# command arguments must be:
#	@BotName
#	@Server
#	@Channel to join (no #)
##
 

if len(sys.argv) < 4:
	sys.exit("You do not have the correct amount of Parameters - name,server,channel(no \'#\')")

todolist = []
bot_name = sys.argv[1]
server = sys.argv[2]
start_channel = '#' + sys.argv[3]


def one_liner(self,string):
    if self.get_channel()[0] == "#":
        self.protocol.privmsg(self.get_channel(), string)
    else:
        self.protocol.privmsg(self.get_username(), string)


#1 - Anywhere in String
def trigger_word(self,trigger,result,option):
    if option == 1:
        if botlib.check_found(self.data, trigger):
            one_liner(self,result)
    else:
        if botlib.check_on_own(self.data, trigger):
            one_liner(self,result)
            

# Create a new class for our bot, extending the Bot class from botlib
class HelloWorldBot(botlib.Bot):
	
    def __init__(self, server, channel, nick, password=None):
        botlib.Bot.__init__(self, server, 6667, channel, nick)
 
        # Send nickserv password if availible
        if password != None:
            self.protocol.privmsg("nickserv", "identify" % password)
 

		
	
    def __actions__(self):

        botlib.Bot.__actions__(self)
        print self.data

        if botlib.check_on_own(self.data, "~join ") or botlib.check_found(self.data, "~j "):
            channel = self.get_args();
            if len(channel) == 0:
                one_liner(self,"Please tell me a channel to go in. I'm not psychic you know!")
            elif channel[0][0] != '#':
                one_liner(self,"# plz")
            else:
                one_liner(self,"Alrighty")
                self.protocol.join(channel[0])
                self.protocol.privmsg(channel[0], "Sup? please blame %s for the proceeding spam!" % self.get_username())

        if botlib.check_found(self.data, "~out"):
            self.protocol.leave(self.get_channel())
        elif botlib.check_on_own(self.data,"~todo"):
            for x in range(len(todolist)):
                if todolist[x][1] == self.get_username():
                    print todolist[x]
                    self.protocol.privmsg(self.get_channel(),todolist[x][0])
        elif botlib.check_on_own(self.data,"~add"):
            what = self.get_args();
            if len(what) > 0 :
                todolist.append((' '.join(what),self.get_username()))
                one_liner(self,"Added.")
        elif botlib.check_on_own(self.data,"~remove"):
            what = self.get_args();
            if(len(what) > 0):
                for x in range(len(todolist)):
                    if todolist[x][1] == self.get_username() and todolist[x][0] == what[0] :
                        todolist.pop(x)
                        one_liner(self,"Removed.")
                        break;

        trigger_word(self,"~~","you rang?", 0)
	trigger_word(self,"~help","https://bitbucket.org/mrpineapple/ianbot2 contribute!",1)
        trigger_word(self,"~test","BZZA^%^&\".", 1)
        trigger_word(self,"~vpn","Have you checked your proxy settings and are running as admin?", 1)
        #Love
        trigger_word(self,"<3 " + bot_name,"<3 you too %s" % self.get_username(), 1)
        trigger_word(self,"Love you " + bot_name,"<3 you too %s" % self.get_username(), 1)
        #Hi
	trigger_word(self,"Hello " + bot_name,"Hi %s :)" % self.get_username(), 1)
 	trigger_word(self,"hello " + bot_name,"Yo %s :D" % self.get_username(), 1)
        trigger_word(self,"Hi " + bot_name,"Wazzup.", 1)
        trigger_word(self,"hi " + bot_name,"Oh! Hey %s" % self.get_username(), 1)
        trigger_word(self,"Sup " + bot_name,"nm, you?", 1)
        trigger_word(self,"bye "+ bot_name,"see ya", 0)
        trigger_word(self,"nn","sweet dreams :3", 0)
        trigger_word(self,"kawaii","^_~", 1)
        trigger_word(self,"csb","please. stop while you're ahead", 1)
        trigger_word(self,"=(","cheer up %s, life ain't so bad" % self.get_username(), 1)        
        if botlib.check_found(self.data, "~weather"):
            result = scrapemark.scrape("""
	        
	                <td width="70" align="center" ><b><font >Minimum</font></b></td> 
	                <td width="70" align="center" ><b><font >Maximum</font></b></td> 
	              </tr> 
	               
	              <tr> 
	                <td width="70" align="center">  
	                </td> 
	                <td width="70" align="center">
	                  <b><font >
	                  {{value_min}}
	                  </font></b>
	                </td> 
	                <td width="70" align="center">
	                  <b><font > 
	                  {{value_max}}
	                  </font></b>
	                </td>
					 
	                <td width="45" height="45" align="center" valign="middle"><b><img alt="{{type}}" width="35" height="35"></b></td> 
			
	        	""",
	        	url='http://www.worldweather.org/067/c00188.htm')	
            one_liner(self,"min {0}, max {1} C - {2}".format(result['value_min'],result['value_max'],result['type']))
        if botlib.check_found(self.data, "~anime"):
             result = scrapemark.scrape("""
                              {*
                               <div>
                                    <h2>Random Anime</h2>
                                    <div class='border'>   
                                    <a class="borderCrop" href=''>
                                        <img alt="" src="" /></a>  
                                    </div>
                                    <h3><a href="">{{title}}</a></h3>
                                    <p>{{description}}
                                    Random Review
                                </div>
                              *}
                                """,
                url='http://www.anime-planet.com') 
             a = unicodedata.normalize('NFKD', result['title']).encode('ascii','ignore')
             b = unicodedata.normalize('NFKD', result['description']).encode('ascii','ignore')
             one_liner(self,(" Random Anime - {0} - {1} ".format(a,b).encode('ascii',"ignore")))
            
        if  botlib.check_found(self.data, "~manga"):
             result = scrapemark.scrape("""
                               <div>
                                    <h2>Random Manga</h2>
                                    <div class=''>   
                                    </div>
                                    <h3><a href="">{{title}}</a></h3>
                                    <p>{{description}} Facebook
                                </div>
                                """,
                url='http://www.anime-planet.com')   

             #Unicode To Ascii Conversions - These were a pain in the arse and a half!
             a = unicodedata.normalize('NFKD', result['title']).encode('ascii','ignore')
             b = unicodedata.normalize('NFKD', result['description']).encode('ascii','ignore')
             one_liner(self,(" Random Manga - {0} - {1} ".format(a,b).encode('ascii',"ignore")))

        if botlib.check_found(self.data, "~movie"):
            try:
                result = scrapemark.scrape("""
                <div><h3>{{title}}</h3><br>
                <b></b><br><br><iframe src="{{video_link}}"></iframe>
                        """,
                        url='http://www.whichmovietowatch.com/light/pick.php')
                self.protocol.privmsg(self.get_channel(), "What about - {0} - Trailer: {1}".format(result['title'],result['video_link']))
            except HTTPError:
                oneliner(self,"nope")
	
        if botlib.check_found(self.data, "~deadlines"):
           	result = scrapemark.scrape("""
			<body>
			{*
                		<p> {{ [deadlines] }} </p>
			*}
			</body>
                	""",
                	url = "http://ohjann.netsoc.ie")
		length = len(result['deadlines'])
		for item in range(length):
			one_liner(self, "{0}".format(result['deadlines'][item]))


        elif botlib.check_found(self.data, "~lol"):
            result = scrapemark.scrape("""
				<ol class="free_champion_rotation" >
				<li><span class="character_icon"><a ><img /></a>&#160;<span><a >{{ChampA}}</a></span></span>
				<li><span class="character_icon"><a ><img /></a>&#160;<span><a >{{ChampB}}</a></span></span>
				<li><span class="character_icon"><a ><img /></a>&#160;<span><a >{{ChampC}}</a></span></span>
				<li><span class="character_icon"><a ><img /></a>&#160;<span><a >{{ChampD}}</a></span></span>
				<li><span class="character_icon"><a ><img /></a>&#160;<span><a >{{ChampE}}</a></span></span>
				<li><span class="character_icon"><a ><img /></a>&#160;<span><a >{{ChampF}}</a></span></span>
				<li><span class="character_icon"><a ><img /></a>&#160;<span><a >{{ChampG}}</a></span></span>
				<li><span class="character_icon"><a ><img /></a>&#160;<span><a >{{ChampH}}</a></span></span>
				<li><span class="character_icon"><a ><img /></a>&#160;<span><a >{{ChampI}}</a></span></span>
				<li><span class="character_icon"><a ><img /></a>&#160;<span><a >{{ChampJ}}</a></span></span>
				</ol>
	        	""",
	        	url='http://leagueoflegends.wikia.com/wiki/Free_champion_rotation')	
            if result != None:
                one_liner(self,"{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}".format(result['ChampA'],result['ChampB'],result['ChampC'],result['ChampD'],result['ChampE'],result['ChampF'],result['ChampG'],result['ChampH'],result['ChampI'],result['ChampJ']))		
            else:
                one_liner(self,"Something went wrong :(")
        elif botlib.check_found(self.data, "~fact"):
            result = scrapemark.scrape("""
                <div id="f">
                <div id='z'>
                {{fact}}
                <br/><iframe></iframe></div><div id='z'>
            """,
                url='http://www.randomfactgenerator.net/') 
            one_liner(self,"{0}".format(result['fact']))     
        elif botlib.check_found(self.data, "~girl"):
            result = scrapemark.scrape("""
                <span style="font-size:14px; color:#347C17">
                {{name}}
                </span><p>
            """,
                url='http://www.thinkbabynames.com/random/0') 
            one_liner(self,"{0}".format(result['name']))     
        elif botlib.check_found(self.data, "~boy"):
            result = scrapemark.scrape("""
                  <span style="font-size:14px; color:#347C17">
                {{name}}
                </span><p>
            """,
                url='http://www.thinkbabynames.com/random/1') 
            one_liner(self,"{0}".format(result['name']))     

        elif botlib.check_found(self.data, "~update"):
            one_liner(self,"Updating")  
            os.system("git pull")
            one_liner(self,"Restarting")  
            sys.exit()

        else:
            if self.count > 500 and self.data.find("PING") == -1 :
                self.count = 0
                num_lines = sum(1 for line in open("sayings.txt"))
                say_position = random.randrange(1,num_lines)
                say_sentence = linecache.getline("sayings.txt", say_position)
                say_sentence = say_sentence.replace("%s", self.get_username())
                one_liner(self,say_sentence)
            elif self.data.find("PING") == -1:
                self.count = self.count + 1
	    else:
		pass
			
if __name__ == "__main__":
    # Create new instance of our bot and run it
    HelloWorldBot(server,start_channel, bot_name).run()
